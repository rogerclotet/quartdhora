#!/bin/sh

(
  sshpass -p $SSH_PASSWORD ssh $SSH_USERNAME@$SSH_IP -o StrictHostKeyChecking=no <<-EOF
    source ~/.bashrc
    cd $SSH_PROJECT_DIRECTORY
    git pull
    nvm install
    nvm use
    yarn install
    yarn build
EOF
)
